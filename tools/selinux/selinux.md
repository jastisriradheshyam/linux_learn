# SE Linux

- Disable the SE Linux rules enforcement for the current session: `sudo setenforce 0`

## References:
- https://www.thegeekdiary.com/how-to-disable-or-set-selinux-to-permissive-mode/