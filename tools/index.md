# Tools

## List

- awk
  - [awk.md](./awk/awk.md)
- brave_browser
  - [brave_browser.md](./brave_browser/brave_browser.md)
- certbot
  - [certbot.md](./certbot/certbot.md)
- curl
  - [curl.md](./curl/curl.md)
- date
  - [date.md](./date/date.md)
- dig-and-nslookup
  - [dig.md](./dig-and-nslookup/dig.md)
  - [nslookup.md](./dig-and-nslookup/nslookup.md)
- docker
  - [docker-compose.md](./docker/docker-compose.md)
  - [docker.md](./docker/docker.md)
  - [dockerfile.md](./docker/dockerfile.md)
  - [registry.md](./docker/registry.md)
  - [secrets.md](./docker/secrets.md)
  - [stack.md](./docker/stack.md)
  - [swarm.md](./docker/swarm.md)
- efibootmgr
  - [efibootmgr.md](./efibootmgr/efibootmgr.md)
- find
  - [find.md](./find/find.md)
- firefox
  - [firefox.md](./firefox/firefox.md)
- firewall
  - [firewall.md](./firewall/firewall.md)
- fsck
  - [fsck.md](./fsck/fsck.md)
- git
  - [git.md](./git/git.md)
- gpg
  - [disable_password_caching.md](./gpg/disable_password_caching.md)
  - [gpg.md](./gpg/gpg.md)
  - [gpg_best_practices.md](./gpg/gpg_best_practices.md)
  - [gpg_faq.md](./gpg/gpg_faq.md)
- grep
  - [grep.md](./grep/grep.md)
- haproxy
  - [haproxy.md](./haproxy/haproxy.md)
- ip
  - [ip.md](./ip/ip.md)
- keepassxc
  - [keepassxc.md](./keepassxc/keepassxc.md)
- kill
  - [kill.md](./kill/kill.md)
- kubernetes
  - [helm.md](./kubernetes/helm.md)
  - [kubernetes.md](./kubernetes/kubernetes.md)
  - [minikube.md](./kubernetes/minikube.md)
  - [subtopics](./kubernetes/subtopics)
    - [deployments.md](./kubernetes/subtopics/deployments.md)
    - [replicas.md](./kubernetes/subtopics/replicas.md)
    - [services.md](./kubernetes/subtopics/services.md)
- make
  - [make.md](./make/make.md)
- mysql
  - [mysql.md](./mysql/mysql.md)
  - [quirks.md](./mysql/quirks.md)
- nc
  - [nc.md](./nc/nc.md)
- openssl
  - [openssl.md](./openssl/openssl.md)
- postgres
  - [postgres.md](./postgres/postgres.md)
- powershell
  - [powershell.md](./powershell/powershell.md)
- ps
  - [ps.md](./ps/ps.md)
- rsync
  - [rsync.md](./rsync/rsync.md)
- screen
  - [screen.md](./screen/screen.md)
- selinux
  - [selinux.md](./selinux/selinux.md)
- ss
  - [ss.md](./ss/ss.md)
- ssh
  - [ssh.md](./ssh/ssh.md)
- sudo
  - [sudoers.md](./sudo/sudoers.md)
- systemd
  - [systemd.md](./systemd/systemd.md)
- tail
  - [tail.md](./tail/tail.md)
- tar
  - [tar.md](./tar/tar.md)
- tilix
  - [tilix.md](./tilix/tilix.md)
- ufw
  - [ufw.md](./ufw/ufw.md)
- vi
  - [vi.md](./vi/vi.md)
- virtual_box
  - [VBoxManage.md](./virtual_box/VBoxManage.md)
- vscode
  - [vscode.md](./vscode/vscode.md)
- watch
  - [watch.md](./watch/watch.md)
- x11vnc
  - [x11vnc.md](./x11vnc/x11vnc.md)
