# Brave Browser

## Tab scrolling

- `brave://flags/#scrollable-tabstrip`
    - Enable : `Tab Scrolling` and `Tab Scrolling Buttons`

## References
- https://community.brave.com/t/tab-bar-scrolling/299185/4