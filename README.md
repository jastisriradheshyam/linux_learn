# Linux Learn

Just a linux reference to myself.

## Linux
- Aims to be POSIX and Single UNIX Specification compliant.
- A kernel

## Directory
- [Tools](./tools/index.md)
- [Resource](./resource/resource.md)

## Common References
- https://www.kernel.org/doc/html/latest/admin-guide/README.html
